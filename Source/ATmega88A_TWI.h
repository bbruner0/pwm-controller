/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/*
 * TWI.h
 *
 * Created: 02/05/2014 2:23:34 PM
 * Author: Brendan Bruner
 * 
 * Notes:
 * Global interrupts must be enabled for this driver to work.
 * This driver implements non reentrant functions
 *
 * Description:
 * This module enables the use of the ATmega88A's TWI. The module will implement
 * master transmitter and master receiver mode only.
 */ 


#ifndef TWI_H_
#define TWI_H_

#include <avr/io.h>
#include <avr/interrupt.h>

/************************************************************************/
/* Return codes                                                         */
/************************************************************************/
/* Return codes from TWI_initialize */
#define TWI_INIT_OK			1	/* TWI initialized ok					*/
#define TWI_INIT_DUPLICATE	2	/* TWI has already been initialized		*/

/* Return codes from TWI_pushPackets */
#define TWI_TXBUF_OK		5	/* Packets pushed successfully			*/
#define TWI_TXBUF_OVERFLOW	6	/* Transmitter stack overflow			*/

/* Return codes from TWI_popPackets */
#define TWI_RXBUF_OK		7	/* Packets popped successfully			*/
#define TWI_RXBUF_OVERFLOW	8	/* Receiver stack overflow				*/

/************************************************************************/
/* Buffer and bit definitions											*/
/************************************************************************/
#define TWI_TXBUF_SIZE	18		/* slave address + r/w and 15 data byte */
#define TWI_RXBUF_SIZE	2		/* Min size of message buffer			*/

#define TWI_RW_BIT		0x01	/* Masks slave address in SLA+R/~W		*/
#define TWI_PS_MASK		0xF8	/* Mask prescale bits in TWSR			*/
#define TWI_STATUS_MASK	0x03	/* Mask all but prescale bits in TWSR	*/

/************************************************************************/
/* Status register return codes (prescalar bits masked)                 */
/* See documentation for explanation of status codes					*/
/************************************************************************/
/* Status codes common to all modes of operation						*/
#define TWI_START		0x08	/* Start condition has been transmitted */
#define TWI_REP_START	0x10	/* Repeated start has been transmitted  */
#define TWI_NO_STATE	0xF8	/* TWINT = 0, no state available		*/
#define TWI_BUS_ERROR	0x00	/* Bus error							*/
#define TWI_LOST_ARB	0x38	/* Lost bus arbitration					*/

/* Master transmitter mode status codes									*/
#define TWI_MTX_ADDR_ACK	0x18	/* SLA+W sent and an ACK received	*/
#define TWI_MTX_ADDR_NACK	0X20	/* SLA+W sent and NACK received		*/
#define TWI_MTX_DATA_ACK	0x28	/* ACK of sent data received		*/
#define TWI_MTX_DATA_NACK	0x30	/* NACK of sent data				*/

/* Master receiver mode status codes									*/
#define TWI_MRX_ADDR_ACK	0x40	/* SLA+R sent and an ACK received	*/
#define TWI_MRX_ADDR_NACK	0x48	/* SLA+R sent and NACK received		*/
#define TWI_MRX_DATA_ACK	0x50	/* Data byte received, ACK sent		*/
#define TWI_MRX_DATA_NACK	0x58	/* Data byte received, NACK sent	*/


/************************************************************************/
/* Data structures for TWI                                              */
/************************************************************************/
struct TWI_stack
{
	/* Points to the bottom of the stack */
	char *bottom;
	/* Points to the top of the stack */
	char *top;
	/* Points to an unused entry in the stack closest to the bottom */
	char *current;
};


/************************************************************************/
/* Functions                                                            */
/************************************************************************/
/* TWI_initialize
 *
 * RETURNS: Returns an error code
 *			TWI_INIT_OK			If initialized ok
 *			TWI_INIT_DUPLICATE	If duplicate initialization was done
 *
 * NOTES:	The SDA, and SCL pins are disconnected from their port when
 *			the TWI is enabled therefore the pins do not need to be 
 *			initialized as output pins.
 *
 * DESCRIPTION: 
 * This function initializes the TWI; it should be the first function 
 * called if the TWI is going to be used.
 */
uint8_t TWI_initialize(uint8_t bitRate, uint8_t prescalar);

/* TWI_setFrequency
 * 
 * RETURNS:	None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Sets the frequency of the bus based on the given bitRate, and 
 * prescalar values.
 */
void TWI_setFrequency(uint8_t bitRate, uint8_t prescalar);

/* TWI_pushPackets
 *
 * RETURNS:	Returns an error code
 *			TWI_TXBUF_OK		if data was pushed successfully
 *			TWI_TXBUF_OVERFLOW	if the pushed data caused an overflow
 *
 * NOTES:	When pushing data causes TWI_TXBUF_OVERFLOW to be returned
 *			the stack is reset to its state before pushing data on it. 
 *
 * DESCRIPTION:
 * This function pushes data onto the transmitter stack, it pushes numPackets
 * data bytes onto the stack from the array packets.
 */
uint8_t TWI_pushPackets(char const *packets, uint8_t numPackets);

/* TWI_popPackets
 * 
 * RETURNS: Returns an error code
 *			TWI_RXBUF_OK		if all packets read are in the Rx stack
 *			TWI_RXBUF_OVERFLOW	if the Rx stack overflowed reading incoming
 *								packets
 * 
 * NOTES:	When this function returns TWI_RXBUF_OVERFLOW all the data in
 *			the Rx stack that was requested is still copied. This simply
 *			tells the programmer there was more data on the bus that wasn't
 *			read.
 *
 * DESCRIPTION:
 * Copies the packets read from the bus into the array packets taken as
 * input. The input numPackets dictates the number of packets that can be
 * read. If numPackets is greater than the size of the Rx stack then all
 * packets are copied.
 */
uint8_t TWI_popPackets(char *packets, uint8_t numPackets);

/* TWI_sendPackets
 *
 * RETURNS:	None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * This function will take what is on the Tx stack and put it on the bus. The
 * Tx stack is a FIFO buffer, so the first byte added to the stack is the first
 * byte put on the bus.
 */
void TWI_sendPackets(void);

/* TWI_clearStack
 * 
 * RETURNS: None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Resets the Tx stack.
 */
void TWI_clearStack();

/* _TWI_wait
 * 
 * RETURNS: None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Waits until the TWI is no longer busy.
 */
//static void _TWI_wait();

/* TWI_TxStackSize
 *
 * RETURNS:	The size of the Tx stack
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Returns how many data bytes fit into the Tx stack.
 */
uint8_t TWI_TxStackSize();

/* TWI_RxStackSize
 *
 * RETURNS:	The size of the Rx stack
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Returns the number of data bytes that will fit into the Rx stack.
 */
uint8_t TWI_RxStackSize();


#endif /* TWI_H_ */
