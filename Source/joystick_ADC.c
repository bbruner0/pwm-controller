/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/* ========================================================================== */
/*																			  */
/* joystick_ADC																  */
/* June 01, 2014 05:15:25 PM 												  */
/* Brendan Bruner															  */
/* 																			  */
/* ========================================================================== */
/*
 * Notes:
 * Functions are in alphabetical order. 
 * Interrupts must be enabled for this module to work.
 *
 * Description:
 * This module uses the ADC hardware to convert analog signals.
 */
 
#include "joystick_ADC.h"

/******************************************************************************/
/* Global variables															  */
/******************************************************************************/

static uint16_t conversionResult;
 
/******************************************************************************/
/* Functions																  */
/******************************************************************************/
/* ADC_convert
 *
 * Notes:
 * Global interrupts must be enabled for any meaningful results to be obtained.
 *
 * Description:
 * This function will start one ADC conversion.
 */
void ADC_convert(void)
{
	ADCSRA |= (1 << ADSC); /* Start a conversion */	
}

/* ADC_initialize
 * 
 * Description:
 * This function initializes the ADC hardware to read from one channel.
 */
void ADC_initialize(void)
{
	static uint8_t ADC_initialized = 0;
	
	/* Prevent duplicate initialization */
	if(ADC_initialized) return;
	ADC_initialized = 1;	
	
	/* Set conversion result to 0 initially. */
	conversionResult = 0;
	
	ADMUX = ADC_CHANNEL |	/* Set channel of ADC hardware */
			ADC_REFV;		/* Set reference voltage of ADC hardware */
			
	ADCSRA = (1 << ADEN) |	/* Enable the ADC hardware */
			 (1 << ADIE) |	/* Enable interrupts when ADC conversions finish */
			 ADC_PRESCALE;	/* Set the prescalar bits */
}

/* ADC_result
 *
 * Returns:
 * an unsigned 16 bit number which is the result of the most recently completed
 * ADC conversion.
 *
 * Description:
 * This function returns the value of the most recent ADC conversion. The most
 * recent conversion may not be the one requested by the most recent call to
 * ADC_convert. This function will return 0 if no ADC conversion has completed
 * when called. 
 */
uint16_t ADC_result(void)
{
	return conversionResult;
}

/******************************************************************************/
/* ISR																		  */
/******************************************************************************/
/* ISR(ADC_vect)
 *
 * Notes:
 * Hardware clears interrupt flag, this does not have to be done by software.
 *
 * Description:
 * The ISR takes the results of an ADC conversion and places it in the memory
 * location pointed to by conversionResult
 */
ISR(ADC_vect)
{
	conversionResult = 0;
	conversionResult = ADCL;
	conversionResult |= ((uint16_t)ADCH << ADC_ONE_BYTE);
}
