/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/* ========================================================================== */
/*																			  */
/* LED_PWM.h																  */
/* May 31, 2014 02:07:27 PM 												  */
/* Brendan Bruner															  */
/* 																			  */
/* ========================================================================== */
/*
 * Notes:
 * Functions are listed in alphabetic order.
 *
 * Description:
 * Controls the brightness of LED's useing PWM. The PWM signal is output on 
 * Port D, pin 6. This is pin 12 on the atmega88A 28 PDIP package. 
 */

#ifndef LED_PWM_
#define LED_PWM_

#include <avr/io.h> 

#define INITIAL_DUTY_CYCLE 128 /* Initial 50% duty cycle of PWM signal */
/******************************************************************************/
/* Functions																  */
/******************************************************************************/

/* PWM_ledDutyCycle
 *
 * Arguments:
 * uint8_t PWM_dutyCycle
 *	A new duty cycle, 0x00 = 0.4% duty cycle, 0xFF = 100% duty cycle.
 *
 * Notes:
 * Since the hardware enables double buffering in fast PWM mode, the duty cycle
 * can be updated at anytime without affecting the current period of the PWM 
 * signal. In otherwords the duty cycle does not get updated in hardware until
 * the next period.
 * 0% duty cycle cannot be achieved in non inverted mode
 *
 * Description:
 * Sets the duty cycle of the PWM signal.
 */
void PWM_ledDutyCycle(uint8_t PWM_dutyCycle);

/* PWM_ledInitialize
 *
 * Description:
 * Initializes the timer/counter hardware to generate a PWM signal
 */ 
void PWM_ledInitialize(void);

#endif /* FILENAME_H_ */
