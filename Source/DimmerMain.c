/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/* ========================================================================== */
/*																			  */
/* DimmerMain.c																  */
/* June 02, 2014 06:55:08 AM 												  */
/* Brendan Bruner															  */
/* 																			  */
/* ========================================================================== */
/*
 * Notes:
 *
 * Description:
 * This module reads the voltage across an ADC channel to generate a PWM  signal
 * with a duty cycle that dims an LED. The brightness of the LED is graphically
 * displayed on an LCD screen.
 */
#define F_CPU 1000000

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include "ATmega88A_TWI.h"
#include "LCD_TWIinterface.h"
#include "joystick_ADC.h"
#include "LED_debug.h"
#include "LED_PWM.h"

#define MAX_ADC_RESULT_SIZE	3		/* Size of string to put ADC result in	*/
#define PRESCALAR			0x00	/* TWI prescalar						*/
#define BIT_RATE			0x02	/* TWI bit rate							*/
#define LCD_TOP_POSITION	0x00	/* Cursor position for the top of LCD	*/
#define LCD_BOTTOM_POSITION	0x10	/* Cursor position for the bottom of LCD*/
#define LCD_LAST_POSITION	0X1F	/* Cursor position for last character	*/
#define SOFT_CLOCK			50		/* Period at which the system loops (ms)*/
#define LCD_SPACE			' '		/* Default char for conversion string	*/
#define LCD_BLOCK			0xFF	/* Block to display on LCD				*/
#define LCD_LENGTH			16		/* Length of one line on LCD screen		*/


int main(void)
{
	/* dimFactor is the result of ADC conversion
	 * topMessage gets displayed on the top line of the LCD screen
	 * bottomMessage gets displayed on the bottom line of the LCD screen
	 * cursorPos is a for loop iterator for what character goes where on screen
	 * blockEnd is cursor position of the last block on the LCD screen
	 */
	uint16_t	dimFactor = 0;
	char const	*topMessage = "Conversion:";
	char 		bottomMessage[LCD_LENGTH];
	int8_t		cursorPos, blockEnd;
	
	/* For debugging */
	LED_initialize();
	LED_greenOn();
	LED_redOff();
	
	/* Initialize the TWI, LCD, and ADC module. The TWI bus is set to 50kHz, and
	 * ADC conversions are set to be stored in the variable dimFactor.
	 */
	TWI_initialize(BIT_RATE, PRESCALAR);
	LCD_initialize();
	ADC_initialize();
	PWM_ledInitialize();
	
	/* The TWI, LCD, and ADC modules requires global interrupts be enabled for
	 * them to work.
	 */
	sei();
	
	LCD_clear();
	LCD_setCursor(LCD_TOP_POSITION);
	LCD_write(topMessage, strlen(topMessage));
	LCD_setCursor(LCD_BOTTOM_POSITION);

	while(1)
	{
		/* Get an ADC value */
		ADC_convert();
		dimFactor = ADC_result();
		
		/* Round that 10 bit ADC value to be in the range of 0 to 15 */
		blockEnd = (int8_t)(dimFactor >> 6);	
		/* Set the characters between the end of the screen (LCD_LENGTH - 1)
		 * and the blockEnd'th character to be spaces.
		 */
		for(cursorPos = LCD_LENGTH - 1; cursorPos > blockEnd; --cursorPos)
		{
			bottomMessage[cursorPos] = LCD_SPACE;
		}
		/* Set the characters at the begining of the screen to the character
		 * before the blockEnd'th character to be blocks.
		 */
		for(cursorPos = blockEnd; cursorPos >= 0; --cursorPos)
		{
			bottomMessage[cursorPos] = LCD_BLOCK;
		}
		
		/* Write the message to the screen, and set the PWM duty cycle */
		LCD_setCursor(LCD_BOTTOM_POSITION);
		LCD_write(bottomMessage, LCD_LENGTH);
		PWM_ledDutyCycle((uint8_t)(dimFactor >> 2));
		_delay_ms(SOFT_CLOCK);
	}
}
