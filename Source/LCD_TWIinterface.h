/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/*
 * LCD_TWIinterface.h
 *
 * Created: 10/05/2014 4:46:08 PM
 * Author: Brendan Bruner
 *
 * NOTES:	This module enables the use of the newhaven LCD display which
 *			uses an I2C protocol. To communicate via an I2C bus this module
 *			uses the TWI module. The TWI module should be initialized before
 *			this module is initialized to have a higher degree of determinism.
 *			In the event the TWI module has not been initialized this module
 *			will initialize it and set the bus frequency to be 50kHz
 *			(this applies to ATmega88A).
 *
 *			Global interrupts must  be enabled for this driver to work.
 *
 *			The newhaven LCD display requires the bus frequency to be less
 *			than 100kHz.
 *
 *			All LCD functions clear the TWI stack, and are non reentrant.
 *
 * DESCRIPTION:
 * This module is a driver for the newhaven LCD display, it must be initialized
 * before other functions are used by calling:
 *		uint8_t LCD_initialize(void)
 * The cursor on the LCD can be set with a call to:
 *		void LCD_setCursor(uint8_t position)
 * where 'position' is in the range of 0x00 - 0x1F.
 * The screen can be written to with a call to:
 *		void LCD_write(char *message, uint8_t msgSize)
 * where 'message' is a string containing what should be written to the
 * screen, and msgSize is the number of characters in that message.
 * The screen can be cleared with a call to:
 *		void LCD_clear(void)
 * which clears all the characters off the screen.
 */


#ifndef LCD_TWIINTERFACE_H_
#define LCD_TWIINTERFACE_H_

#define F_CPU 1000000

#include <avr/io.h>
#include <util/delay.h>
#include "ATmega88A_TWI.h"

/************************************************************************/
/* Macros for communicating with LCD screen                             */
/************************************************************************/
#define LCD_LINE1_END	 0x0F	/* End of line 1 0x00 - 0x0F			*/
#define LCD_LINE2_END	 0x4F	/* End of line 2 0x40 - 0x4F			*/
#define LCD_LINE2_OFFSET 0x30	/* Off set added to cursor position when*/
								/* on second line						*/ 
#define LCD_TWI_BITRATE	 0x02	/* Bit rate for TWI bus					*/
#define LCD_TWI_PRESCALE 0x00	/* Prescalar for TWI bus				*/
#define LCD_SLAVEADDRESS 0x50	/* Slave address of the LCD screen		*/
#define LCD_INIT_LENGTH	 11		/* Number of bytes to put on bus to		*/
								/* initialize LCD screen				*/
#define LCD_SETCURS_LEN	 4		/* Number of packets used in setCursor	*/
#define LCD_CLEAR_LEN	 3		/* Number of packets used to clear		*/
#define LCD_MAX_PACKET_LEN	17	/* The maximum number of packets the LCD*/
								/* driver will ever put on the TWI bus	*/
								
#define LCD_PREFIX			0xFE	/* Prefix for LCD command			*/
#define LCD_SET_CURSOR		0x45	/* Set cursor command				*/
#define LCD_UNDERLINE_OFF	0x48	/* Underline cursor off				*/
#define LCD_BLINKING_OFF	0x4C	/* Blinking cursor off				*/
#define LCD_CLEAR_SCREEN	0x51	/* Clear LCD screen					*/
#define LCD_SET_CONTRAST	0x52	/* Set contrast of LCD screen		*/
#define LCD_SET_BACKLIGHT	0x53	/* Set backlight brightness			*/
#define LCD_CONTRAST		40		/* LCD contrast level				*/
#define LCD_BRIGHTNESS		8		/* LCD brightness level				*/


/************************************************************************/
/* Return codes                                                         */
/************************************************************************/
#define LCD_INIT_OK					1	/* LCD initialized ok			*/
#define LCD_INIT_TWIFREQ_CHANGED	2	/* LCD initialized ok, but TWI	*/
										/* frequency was changed		*/
#define LCD_INIT_DUPLICATE			3	/* If the LCD is already init	*/
#define LCD_INIT_OVERFLOW			4	/* If the driver cannot write	*/
										/* 16 characters at a time to	*/
										/* the LCD screen				*/

/************************************************************************/
/* Macros to simplify code                                              */
/************************************************************************/
#define CursorPos(a) (((a) > LCD_LINE1_END)\
					 ? ((a) + LCD_LINE2_OFFSET)\
					 : (a))


/************************************************************************/
/* Functions                                                            */
/************************************************************************/
/* LCD_initialize
 *
 * RETURNS:	Returns an error code
 *			LCD_INIT_OK					if initialized ok
 *			LCD_INIT_TWIFREQ_CHANGED	if TWI frequency was changed when
 *										initializing
 *			LCD_INIT_DUPLICATE			if the LCD is already initialized
 *			LCD_INIT_OVERFLOW			if the LCD uses more memory than the
 *										TWI stack has
 *
 * NOTES:	The TWI hardware should be initialized before initializing the
 *			LCD. If the TWI hardware is not initialized first then this
 *			function will initialize it with a bit rate of 0x02, and 
 *			prescalar of 0x00. On the ATmega88A this gives a bus
 *			frequency of 50kHz. 
 *
 * DESCRIPTION:
 * Initializes the LCD screen
 */
uint8_t LCD_initialize(void);

/* LCD_setCursor
 *
 * RETURNS:	None
 *
 * NOTES:	Do not use 0x40-0x4F for the bottom line as the documentation
 *			specifies, this function will adjust the position to follow
 *			the documentation.
 *
 * DESCRIPTION:
 * Sets the position of the cursor on the LCD screen. 0x00 - 0x0F is on
 * the top line, and 0x10 - 0x1F is on the bottom line.
 */
void LCD_setCursor(uint8_t position);

/* LCD_write
 * 
 * RETURNS:	None
 *
 * NOTES:	If the string message contains more characters than the LCD 
 *			screen can display then only the first 32 characters of the
 *			string are displayed, and the rest are ignored.
 *
 * DESCRIPTION:
 * Writes the characters in the string message to the LCD screen. msgSize
 * is how many characters are in the string message.
 */
void LCD_write(char const *message, uint8_t msgSize);

/* LCD_clear
 *
 * RETURNS:	None
 *
 * NOTES:	It takes 1.5ms to clear the LCD screen. In some cased it may be
 *			quicker to write blank spaces to the LCD screen when there is a
 *			small amount of text requiring clearing. This delay does not
 *			account for the time it takes the data to arrive at the LCD.
 *			This function delays for 3ms to account for the delay in clearing
 *			The screen.
 *
 * DESCRIPTION:
 * Clears all text from the LCD screen.
 */
void LCD_clear(void);


#endif /* LCD_TWIINTERFACE_H_ */
