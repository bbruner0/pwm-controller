/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/*
 * ATmega88A_TWI.c
 *
 * Created: 08/05/2014 4:46:26 PM
 * Author:	Brendan
 *
 * Notes:
 * Global interrupts must be enabled for this driver to work.
 * This driver implements non reentrant functions
 *
 * Description:
 * This module enables the use of the ATmega88A's TWI. The module will implement 
 * master transmitter and master receiver mode only.
 */ 

#include "ATmega88A_TWI.h"

/************************************************************************/
/* Global variables                                                     */
/************************************************************************/
static char TWI_TxBuffer[TWI_TXBUF_SIZE];
static char TWI_RxBuffer[TWI_RXBUF_SIZE];

static struct TWI_stack TWI_TxStack;
static struct TWI_stack TWI_RxStack;

static uint8_t TWI_RxStackError;


/************************************************************************/
/* Functions                                                            */
/************************************************************************/
/* _TWI_wait
 * 
 * RETURNS: None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Waits until the TWI is no longer busy.
 */
static void _TWI_wait()
{
	/* Wait while the TWI interrupts are enabled */
	while(TWCR & (1<<TWIE));
}

/* TWI_initialize
 *
 * RETURNS: Returns an error code
 *			TWI_INIT_OK			If initialized ok
 *			TWI_INIT_DUPLICATE	If duplicate initialization was done
 *
 * NOTES:	The SDA, and SCL pins are disconnected from their port when
 *			the TWI is enabled therefore the pins do not need to be 
 *			initialized as output pins.
 *
 * DESCRIPTION: 
 * This function initializes the TWI; it should be the first function 
 * called if the TWI is going to be used.
 */
uint8_t TWI_initialize(uint8_t bitRate, uint8_t prescalar)
{
	static uint8_t initialized = 0;
	
	if(initialized) return TWI_INIT_DUPLICATE;
	initialized = 1;

	/* Initialize transmitter stack */
	TWI_TxStack.bottom		= TWI_TxBuffer;
	TWI_TxStack.current		= TWI_TxBuffer;
	TWI_TxStack.top			= TWI_TxBuffer + TWI_TXBUF_SIZE - 1;
	
	/* Initialize receiver stack */
	TWI_RxStack.bottom		= TWI_RxBuffer;
	TWI_RxStack.current		= TWI_RxBuffer;
	TWI_RxStack.top			= TWI_RxBuffer + TWI_RXBUF_SIZE - 1;
	/* Initialize receiver stack error code */
	TWI_RxStackError	= TWI_RXBUF_OK;
	
	/* Initialize TWI hardware */
	TWBR =	bitRate;						/* Set bit rate				*/
	TWSR &=	~((1<<TWPS0) | (1<<TWPS1));		/* Set prescalar to 0b00	*/
	TWSR |= (TWI_STATUS_MASK & prescalar);	/* Prescalar to prescalar	*/
	TWCR =	(1<<TWEN);						/* Enable TWI hardware,		*/
											/* everything else zero		*/
	return TWI_INIT_OK;
}

/* TWI_setFrequency
 * 
 * RETURNS:	None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Sets the frequency of the bus based on the given bitRate, and 
 * prescalar values.
 */
void TWI_setFrequency(uint8_t bitRate, uint8_t prescalar)
{
	/* Wait until current bus transmission is finished before changing the */
	/* frequency of the bus */
	_TWI_wait();
	
	TWBR =	bitRate;						/* Set bit rate				*/
	TWSR &=	~((1<<TWPS0) | (1<<TWPS1));		/* Set prescalar to 0b00	*/
	TWSR |= (TWI_STATUS_MASK & prescalar);	/* Prescalar to prescalar	*/	
}

/* TWI_pushPackets
 *
 * RETURNS:	Returns an error code
 *			TWI_TXBUF_OK		if data was pushed successfully
 *			TWI_TXBUF_OVERFLOW	if the pushed data caused an overflow
 *
 * NOTES:	When pushing data causes TWI_TXBUF_OVERFLOW to be returned
 *			no data is put on the stack.
 *			This function blocks until the TWI hardware is done using 
 *			the bus. 
 *
 * DESCRIPTION:
 * This function pushes data onto the transmitter stack, it pushes numPackets
 * data bytes onto the stack from the array packets.
 */
uint8_t TWI_pushPackets(char const *packets, uint8_t numPackets)
{
	int i; /* For loop iterator */
	
	/* Check for overflow */
	if(TWI_TxStack.current - TWI_TxStack.bottom + numPackets > TWI_TXBUF_SIZE)
	{
		return TWI_TXBUF_OVERFLOW;
	}
	
	/* Wait until the TWI hardware is no longer using the bus before changing */
	/* the Tx stack. */
	_TWI_wait();
	
	for(i = 0; i < numPackets; i++)
	{
		*TWI_TxStack.current = packets[i];
		TWI_TxStack.current++;
	}
	
	return TWI_TXBUF_OK;
}

/* TWI_popPackets
 * 
 * RETURNS: Returns an error code
 *			TWI_RXBUF_OK		if all packets read are in the Rx stack
 *			TWI_RXBUF_OVERFLOW	if the Rx stack overflowed reading incoming
 *								packets
 * 
 * NOTES:	When this function returns TWI_RXBUF_OVERFLOW all the data in
 *			the Rx stack that was requested is still copied. This simply
 *			tells the programmer there was more data on the bus that wasn't
 *			read.
 *
 * DESCRIPTION:
 * Copies the packets read from the bus into the array packets taken as
 * input. The input numPackets dictates the number of packets that can be
 * read. If numPackets is greater than the size of the Rx stack then all
 * packets are copied.
 */
uint8_t TWI_popPackets(char *packets, uint8_t numPackets)
{
	int i; /* For loop iterator */
	
	/* Wait for TWI hardware to finish with bus before reading from the */
	/* Rx stack. */
	_TWI_wait();
	
	/* Set numPackets to be equal to the Rx stack size if it was greater */
	/* than Rx stack size. */
	if(numPackets > TWI_RXBUF_SIZE) numPackets = TWI_RXBUF_SIZE;
	
	/* Set the current location in the stack to be at the bottom to remove */
	/* data in a FIFO order */
	TWI_RxStack.current = TWI_RxStack.bottom;
	
	/* Copy data from the Rx stack into the array packets */
	for(i = 0; i < numPackets; i++)
	{
		packets[i] = *TWI_RxStack.current;
		TWI_RxStack.current++;
	}
	
	return TWI_RxStackError;
}							

/* TWI_clearStack
 * 
 * RETURNS: None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Resets the Tx stack.
 */
void TWI_clearStack(void)
{
	/* Wait until TWI hardware is finished with the Tx stack before clearing */
	/* it */
	_TWI_wait();
	
	/* Adjust current pointer to bottom of the stack */
	TWI_TxStack.current = TWI_TxStack.bottom;
}

/* TWI_sendPackets
 *
 * RETURNS:	None
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * This function will take what is on the Tx stack and put it on the bus. The
 * Tx stack is a FIFO buffer, so the first byte added to the stack is the first
 * byte put on the bus.
 */
void TWI_sendPackets(void)
{
	/* Do nothing if there is no data on the Tx stack */
	if(TWI_TxStack.current - TWI_TxStack.bottom < 1) return;
	
	/* Wait for TWI hardware to finish with the bus lines */
	_TWI_wait();
	
	/* Set the Rx stack error to be OK */
	TWI_RxStackError = TWI_RXBUF_OK;
	
	/* Send a START condition on bus to start data transfer */
	TWCR =	(1<<TWSTA) |				/* Generate start condition		*/
			(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
			(1<<TWEN);					/* Enable TWI hardware			*/
}

/* TWI_TxStackSize
 *
 * RETURNS:	The size of the Tx stack
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Returns how many data bytes fit into the Tx stack.
 */
uint8_t TWI_TxStackSize() 
{
	return TWI_TXBUF_SIZE;
}

/* TWI_RxStackSize
 *
 * RETURNS:	The size of the Rx stack
 *
 * NOTES:	None
 *
 * DESCRIPTION:
 * Returns the number of data bytes that will fit into the Rx stack.
 */
uint8_t TWI_RxStackSize() 
{
	return TWI_RXBUF_SIZE;
}


/************************************************************************/
/* Interrupt Service Routine                                            */
/************************************************************************/
/* ISR
 *
 * NOTES:	If global interrupts are disabled the TWI will not function
 * 
 * DESCRIPTION: 
 * When global interrupts are enabled this runs every time there is a TWI
 * event. The ISR deals with the status codes in the TWSR and continues
 * the transfer of data between slave and master. 
 */
ISR(TWI_vect)
{
	/* Switch status code, and mask prescale bits */
	switch(TWSR & TWI_PS_MASK)
	{
		/************************************************************************/
		/* Status codes common to all modes of operation                        */
		/************************************************************************/
		case TWI_START:
			/* Start condition has been sent, SLA+R/~W is sent next	*/
			TWDR = *TWI_TxStack.bottom;
			/* Increment bottom of stack */
			TWI_TxStack.bottom++;
			
			TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
					(1<<TWEN);					/* Enable TWI hardware			*/
			break;
			
		case TWI_REP_START:	
			/* Repeated start has been sent, SLA+R/~W is sent next */
			TWDR = *TWI_TxStack.bottom;
			/* Increment bottom of stack */
			TWI_TxStack.bottom++;

			TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
					(1<<TWEN);					/* Enable TWI hardware			*/
			break;
			
		case TWI_NO_STATE:
			break;
	
		case TWI_BUS_ERROR:
			break;

		case TWI_LOST_ARB:
			/* Bus arbitration lost, send start condition when TWI is idle */
			/* Reset bottom of the Tx stack */
			TWI_TxStack.bottom = TWI_TxBuffer;
			
			TWCR =	(1<<TWSTA) |				/* Generate start condition		*/
					(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
					(1<<TWEN);					/* Enable TWI hardware			*/
			break;

		/************************************************************************/
		/* Master transmitter mode status codes                                 */
		/************************************************************************/
		case TWI_MTX_ADDR_ACK:
			/* Address has been acknowledged, check if there is data to send */
			if(TWI_TxStack.current - TWI_TxStack.bottom > 0)
			{
				/* There is more data in the Tx stack */
				TWDR = *TWI_TxStack.bottom;
				TWI_TxStack.bottom++;
				
				TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
						(1<<TWEN);					/* Enable TWI hardware			*/
			}
			else
			{
				/* Reset bottom of stack then send STOP condition */
				TWI_TxStack.bottom = TWI_TxBuffer;
				
				/* Send stop condition */
				TWCR =	(1<<TWSTO) |				/* Generate start condition		*/
						(1<<TWINT) | (0<<TWIE) |	/* Start TWI, disable interrupts*/
						(1<<TWEN);					/* Enable TWI hardware			*/	
			}
			break;
			 
		case TWI_MTX_ADDR_NACK:
			/* Address not acknowledged, reset bottom of Tx stack send a repeated */
			/* start */
			TWI_TxStack.bottom = TWI_TxBuffer;
			
			TWCR =	(1<<TWSTA) |				/* Generate start condition		*/
					(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
					(1<<TWEN);					/* Enable TWI hardware			*/
			break;
					
		case TWI_MTX_DATA_ACK:
			/* Sent data acknowledged, send next byte, or stop condition */
			if(TWI_TxStack.current - TWI_TxStack.bottom > 0)
			{
				/* There is more data in the Tx stack */
				TWDR = *TWI_TxStack.bottom;
				TWI_TxStack.bottom++;
				
				TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
						(1<<TWEN);					/* Enable TWI hardware			*/
			}
			else
			{
				/* Reset bottom of stack then send STOP condition */
				TWI_TxStack.bottom = TWI_TxBuffer;
				
				/* Send stop condition */
				TWCR =	(1<<TWSTO) |				/* Generate start condition		*/
						(1<<TWINT) | (0<<TWIE) |	/* Start TWI, disable interrupts*/
						(1<<TWEN);					/* Enable TWI hardware			*/
			}
			break;
			
		case TWI_MTX_DATA_NACK:
			/* Sent data not acknowledged, resend data */
			TWDR = *(TWI_TxStack.bottom - 1);
				
			TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
					(1<<TWEN);					/* Enable TWI hardware			*/
			break;
					
		/************************************************************************/
		/* Master receiver mode status codes                                    */
		/************************************************************************/
		case TWI_MRX_ADDR_ACK:
			/* Address has been acknowledged, read data sent to master */
			if(TWI_RxStack.top - TWI_RxStack.current >= 0)
			{
				/* There is more room in Rx stack for data */
				*TWI_RxStack.current = TWDR;
				TWI_RxStack.current++;
				
				TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
						(1<<TWEN) | (1<<TWEA);		/* Enable TWI hardware, and ACK	*/
			}
			else
			{
				/* Send NACK */
				TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
						(1<<TWEN);					/* Enable TWI hardware			*/	
			}
			break;
						
		case TWI_MRX_ADDR_NACK:
			/* Address not acknowledged, reset Tx stack, send a repeated start */
			TWI_TxStack.bottom = TWI_TxBuffer;
			
			TWCR =	(1<<TWSTA) |				/* Generate start condition		*/
					(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
					(1<<TWEN);					/* Enable TWI hardware			*/	
			break;
					
		case TWI_MRX_DATA_ACK:
			/* Data received, send ACK, or NACK */
			if(TWI_RxStack.top - TWI_RxStack.current >= 0)
			{
				/* There is more room in Rx stack for data */
				*TWI_RxStack.current = TWDR;
				TWI_RxStack.current++;
				
				TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
						(1<<TWEN) | (1<<TWEA);		/* Enable TWI hardware, and ACK	*/
			}
			else
			{
				/* Send NACK */
				TWCR =	(1<<TWINT) | (1<<TWIE) |	/* Start TWI, enable interrupts */
						(1<<TWEN);					/* Enable TWI hardware			*/
			}
			break;
			
		case TWI_MRX_DATA_NACK:
			/* NACK sent, reset Tx stack, send stop condition */
			TWI_TxStack.bottom = TWI_TxBuffer;
			
			TWCR =	(1<<TWSTO) |				/* Generate stop condition		*/
					(1<<TWINT) | (0<<TWIE) |	/* Start TWI, disable interrupts*/
					(1<<TWEN);					/* Enable TWI hardware			*/
			break;

		default:
			break;
	}
}
