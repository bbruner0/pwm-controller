/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/* ========================================================================== */
/*																			  */
/* joystick_ADC.h															  */
/* May 31, 2014 01:27:21 PM 												  */
/* Brendan Bruner															  */
/* 																			  */
/* ========================================================================== */
/*
 * Notes:
 * Functions are in alphabetical order. 
 * Interrupts must be enabled for this module to work.
 *
 * Description:
 * This module uses the ADC hardware to convert analog signals.
 */

#ifndef JOYSTICK_ADC_
#define JOYSTICK_ADC_

#include <avr/io.h>
#include <avr/interrupt.h>

/******************************************************************************/
/* Constants																  */
/******************************************************************************/

#define ADC_PRESCALE (uint8_t)0x03	/* Prescale for ADC clock */
#define ADC_CHANNEL	 (uint8_t)0x00	/* Channel to read signal from */
#define ADC_REFV	 (uint8_t)0x00	/* Reference voltage for ADC */
#define ADC_ONE_BYTE 8				/* Size of one byte */

/******************************************************************************/
/* Function prototypes														  */
/******************************************************************************/
/* ADC_convert
 *
 * Notes:
 * Global interrupts must be enabled for any meaningful results to be obtained.
 *
 * Description:
 * This function will start one ADC conversion.
 */
void ADC_convert(void);

/* ADC_initialize
 * 
 * Description:
 * This function initializes the ADC hardware to read from one channel.
 */
void ADC_initialize(void);

/* ADC_result
 *
 * Returns:
 * an unsigned 16 bit number which is the result of the most recently completed
 * ADC conversion.
 *
 * Description:
 * This function returns the value of the most recent ADC conversion. The most
 * recent conversion may not be the one requested by the most recent call to
 * ADC_convert. This function will return 0 if no ADC conversion has completed
 * when called. 
 */
uint16_t ADC_result(void);

#endif /* FILENAME_H_ */
