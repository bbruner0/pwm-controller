/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/*
 * LCD_TWIinterface.c
 *
 * Created: 11/05/2014 5:07:44 PM
 * Author: Brendan Bruner
 *
 * NOTES:	This module enables the use of the newhaven LCD display which 
 *			uses an I2C protocol. To communicate via an I2C bus this module 
 *			uses the TWI module. The TWI module should be initialized before 
 *			this module is initialized to have a higher degree of determinism. 
 *			In the event the TWI module has not been initialized this module 
 *			will initialize it and set the bus frequency to be 50kHz 
 *			(this applies to ATmega88A).
 *
 *			Global interrupts must  be enabled for this driver to work.
 *
 *			The newhaven LCD display requires the bus frequency to be less
 *			than 100kHz.
 *
 *			All LCD functions clear the TWI stack, and are non reentrant.
 *
 * DESCRIPTION:
 * This module is a driver for the newhaven LCD display, it must be initialized
 * before other functions are used by calling:
 *		void LCD_initialize(void)
 * The cursor on the LCD can be set with a call to:
 *		void LCD_setCursor(uint8_t position)
 * where 'position' is in the range of 0x00 - 0x1F.
 * The screen can be written to with a call to:
 *		void LCD_write(char *message, uint8_t msgSize)
 * where 'message' is a string containing what should be written to the 
 * screen, and msgSize is the number of characters in that message.
 * The screen can be cleared with a call to:
 *		void LCD_clear(void)
 * which clears all the characters off the screen.
 */ 

#include "LCD_TWIinterface.h"

/************************************************************************/
/* Functions                                                            */
/************************************************************************/
/* LCD_initialize
 *
 * RETURNS:	Returns an error code
 *			LCD_INIT_OK					if initialized ok
 *			LCD_INIT_TWIFREQ_CHANGED	if TWI frequency was changed when
 *										initializing
 *			LCD_INIT_DUPLICATE			if the LCD is already initialized
 *			LCD_INIT_OVERFLOW			if the LCD uses more memory than the
 *										TWI stack has
 *
 * NOTES:	The TWI hardware should be initialized before initializing the
 *			LCD. If the TWI hardware is not initialized first then this
 *			function will initialize it with a bit rate of 0x02, and 
 *			prescalar of 0x00. On the ATmega88A this gives a bus
 *			frequency of 50kHz. 
 *
 * DESCRIPTION:
 * Initializes the LCD screen
 */
uint8_t LCD_initialize(void)
{
	static uint8_t	initialized = 0;
	uint8_t			LCD_returnStatus;
	char			LCD_initCodes[LCD_INIT_LENGTH];
	
	if(initialized) return LCD_INIT_DUPLICATE;
	initialized = 1;
	
	if(TWI_TxStackSize() < LCD_MAX_PACKET_LEN) return LCD_INIT_OVERFLOW;
	
	/* Init TWI hardware, and determine return value of this function */
	if(TWI_initialize(LCD_TWI_BITRATE, LCD_TWI_PRESCALE) == TWI_INIT_DUPLICATE)
	{
		 /* TWI was already initialized, its frequency was not changed then */
		 LCD_returnStatus = LCD_INIT_OK; 	
	}
	else LCD_returnStatus = LCD_INIT_TWIFREQ_CHANGED;
	
	/* Put slave address into LCD_initcodes */
	LCD_initCodes[0] = LCD_SLAVEADDRESS;
	
	/* Set the contrast of the LCD */
	LCD_initCodes[1] = LCD_PREFIX;
	LCD_initCodes[2] = LCD_SET_CONTRAST;
	LCD_initCodes[3] = LCD_CONTRAST;
	
	/* Set the brightness of the LCD */
	LCD_initCodes[4] = LCD_PREFIX;
	LCD_initCodes[5] = LCD_SET_BACKLIGHT;
	LCD_initCodes[6] = LCD_BRIGHTNESS;
	
	/* Turn blinking, and underlined cursor off */
	LCD_initCodes[7] = LCD_PREFIX;
	LCD_initCodes[8] = LCD_BLINKING_OFF;
	LCD_initCodes[9] = LCD_PREFIX;
	LCD_initCodes[10] = LCD_UNDERLINE_OFF;
	
	/* Clear the TWI stack of any previous data */
	/* Push the initialization sequence onto the TWI stack, and send it */
	TWI_clearStack();
	TWI_pushPackets(LCD_initCodes, LCD_INIT_LENGTH);
	TWI_sendPackets();
	
	return LCD_returnStatus;
	
}

/* LCD_setCursor
 *
 * RETURNS:	None
 *
 * NOTES:	Do not use 0x40-0x4F for the bottom line as the documentation
 *			specifies, this function will adjust the position to follow
 *			the documentation. 
 *
 * DESCRIPTION:
 * Sets the position of the cursor on the LCD screen. 0x00 - 0x0F is on
 * the top line, and 0x10 - 0x1F is on the bottom line.
 */
void LCD_setCursor(uint8_t position)
{
	char LCD_cursorCodes[LCD_SETCURS_LEN];
	
	/* Adjust position to place cursor where it should go */
	position = CursorPos(position);
	
	/* Set up the packets to send to the LCD which change the position */
	LCD_cursorCodes[0] = LCD_SLAVEADDRESS;
	LCD_cursorCodes[1] = LCD_PREFIX;
	LCD_cursorCodes[2] = LCD_SET_CURSOR;
	LCD_cursorCodes[3] = (char) position;
	
	/* Clear TWI stack, push the packets on the TWI stack, and send them */
	TWI_clearStack();
	TWI_pushPackets(LCD_cursorCodes, LCD_SETCURS_LEN);
	TWI_sendPackets();
}

/* LCD_write
 * 
 * RETURNS:	None
 *
 * NOTES:	If the string message contains more characters than the LCD 
 *			screen can display then only the first 32 characters of the
 *			string are displayed, and the rest are ignored.
 *
 * DESCRIPTION:
 * Writes the characters in the string message to the LCD screen. msgSize
 * is how many characters are in the string message.
 */
void LCD_write(char const *message, uint8_t msgSize)
{
	char LCD_address[1];
	
	/* Clear TWI stack of previous packets */
	TWI_clearStack();
	
	/* Push the slave address onto the TWI stack */
	LCD_address[0] = LCD_SLAVEADDRESS;
	TWI_pushPackets(LCD_address, 1);
	
	/* Push the message onto the TWI stack and send it */
	TWI_pushPackets(message, msgSize);
	TWI_sendPackets();
}

/* LCD_clear
 *
 * RETURNS:	None
 *
 * NOTES:	It takes 1.5ms to clear the LCD screen. In some cased it may be
 *			quicker to write blank spaces to the LCD screen when there is a
 *			small amount of text requiring clearing. This delay does not 
 *			account for the time it takes the data to arrive at the LCD.
 *			This function delays for 3ms to account for the delay in clearing
 *			The screen.
 *
 * DESCRIPTION:
 * Clears all text from the LCD screen.
 */
void LCD_clear(void)
{
	char LCD_clearCodes[LCD_CLEAR_LEN];
	
	/* Set codes in LCD_clearCodes array */
	LCD_clearCodes[0] = LCD_SLAVEADDRESS;
	LCD_clearCodes[1] = LCD_PREFIX;
	LCD_clearCodes[2] = LCD_CLEAR_SCREEN;
	
	/* Clear TWI stack, push packets on it, then send */
	TWI_clearStack();
	TWI_pushPackets(LCD_clearCodes, LCD_CLEAR_LEN);
	TWI_sendPackets();	
	_delay_ms(3);
}
