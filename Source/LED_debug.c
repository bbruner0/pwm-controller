/* Copyright 2014 Brendan Bruner
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * bbruner@ualberta.ca
 */
/*************************************************************************
 * LED_debug.c
 * =====================================================================
 * Date: May 20, 2014
 * Author: Brendan Bruner
 * =====================================================================
 * NOTES:	None
 *
 * DESCRIPTION:
 * This module is used to turn on, and off two LED's.
 */
 
 #include "LED_debug.h"
 
  /* LED_initialize
  *
  * RETURNS: None
  * NOTES:	 None
  *
  * DESCRIPTION:
  * Initialize the red, and green LED.
  */
  void LED_initialize()
  {
	/* Set up PORT B, pins 0, and 1 to be output */
	DDRB	|= (1<<DDB0) | (1<<DDB1);
	/* Set pins 0, and 1 to be high (LED's off) */
	PORTB	|= (1<<PB0) | (1<<PB1);
  }
